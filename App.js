/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {
  Component
} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  YellowBox
} from 'react-native';
import {
  HomeScreen,
  DetailsScreen
} from './components/Screens';
import {
  StackNavigator
} from 'react-navigation';

console.disableYellowBox = true;

export const Events = [{
    nama: 'CompFest X',
    penyelenggara: 'Fasilkom',
    deskripsi: 'Lorem  vel imperdiet neque nibh in est. Pellentesque iaculis, ex quis mattis interdum, lacus nibh dictum dui, in pulvinar odio libero eu quam. Nulla sodales fermentum elit, gravida fermentum lectus. Nullam sed quam a elit elementum bibendum eu et nulla. Morbi eleifend luctus augue. Morbi tincidunt sagittis quam.',
    img: require('./static/img/compfestx.jpg'),
    date: '2018-04-27'
  },
  {
    nama: 'JGTC',
    penyelenggara: 'FEB',
    deskripsi: 'Morbi ornare, tortor a consectetur cursus, quam nisi commodo mi, vel imperdiet neque nibh in est. Pellentesque iaculis, ex quis mattis interdum, lacus nibh dictum dui, in pulvinar odio libero eu quam.',
    img: require('./static/img/jgtc.jpg'),
    date: '2018-08-30'
  },
  {
    nama: 'Olim UI',
    penyelenggara: 'UI',
    deskripsi: 'Nulla sodales fermentum elit, gravida fermentum lectus. Nullam sed quam a elit elementum bibendum eu et nulla. Morbi eleifend luctus augue. Morbi tincidunt sagittis quam.',
    img: require('./static/img/olim.jpg'),
    date: '2018-09-30'
  },
  {
    nama: 'CS League',
    penyelenggara: 'Fasilkom',
    deskripsi: 'Morbi ornare, tortor a consectetur cursus, quam nisi commodo mi, vel imperdiet neque nibh in est. Pellentesque iaculis, ex quis mattis interdum, lacus nibh dictum dui, in pulvinar odio libero eu quam',
    img: require('./static/img/csl.jpg'),
    date: '2018-02-27'
  }
].sort(e => new Date() - new Date(e.date));

export const RootStack = StackNavigator({
  Home: {
    screen: HomeScreen
  },
  Details: {
    screen: DetailsScreen
  }
}, {
  initialRouteName: 'Home',
  navigationOptions: {
    headerStyle: {
      backgroundColor: '#f4511e'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold'
    }
  }
});

export default class App extends Component {
  render() {
    return <RootStack / > ;
  }
}