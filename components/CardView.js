import React, { Component } from 'react';
import {
  StyleSheet,
  AppRegistry,
  Text,
  View,
  TouchableNativeFeedback
} from 'react-native';
import { CardViewStyles } from './styles';

export class CardView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableNativeFeedback useForeground={true} onPress={this.props.onPress}>
        <View style={[this.props.style, CardViewStyles.card]}>
          {this.props.children}
        </View>
      </TouchableNativeFeedback>
    );
  }
}

export class CardTitle extends React.Component {
  constructor(props) {
    super(props);
    this.className = 'CardTitle';
    this.state = {
      value: this.props.children
    };
  }

  render() {
    return (
      <Text
        numberOfLines={1}
        style={[this.props.style, CardViewStyles.title, CardViewStyles.text]}
      >
        {this.state.value}
      </Text>
    );
  }
}

export class CardContent extends React.Component {
  constructor(props) {
    super(props);
    this.className = 'CardContent';
    this.state = {
      value: this.props.children
    };
  }

  render() {
    return (
      <Text numberOfLines={5} style={[this.props.style, CardViewStyles.text]}>
        {this.state.value}
      </Text>
    );
  }
}
