import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import {
  Text,
  View,
  Image,
  FlatList,
  ScrollView,
  Picker,
  TouchableHighlight,
  ToastAndroid
} from 'react-native';

import { CardView, CardTitle, CardContent } from './CardView';
import { DetailStyles } from './styles';
import Swipeout from 'react-native-swipeout';
import { Events, RootStack } from '../App';

export class HomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      filter: 'All',
      events: Events
    };
  }

  static navigationOptions = {
    title: 'Home'
  };

  filterList = filterer => {
    if (filterer == 'All') {
      console.log('HEHE')
      this.setState({
        filter: filterer,
        events: Events
      });
    } else {
      this.setState({
        filter: filterer,
        events: Events.filter(event => event.penyelenggara == filterer)
      });
      console.log('hehe', Events.filter(event => event.penyelenggara == filterer));
    }
  };

  render() {
    let swipeBtns = [
      {
        text: 'Book',
        backgroundColor: '#f4eded',
        underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
        onPress: () => {
          ToastAndroid.show('Event Booked', ToastAndroid.SHORT);
        },
        color: 'black'
      }
    ];
    return (
      <View
        style={{
          overflow: 'scroll'
        }}
      >
        <Picker
          selectedValue={this.state.filter}
          onValueChange={this.filterList}
        >
          <Picker.Item label="All" value="All" />
          <Picker.Item label="Universitas Indonesia" value="UI" />
          <Picker.Item label="Fakultas Ilmu Komputer" value="Fasilkom" />
          <Picker.Item label="Fakultas Ekonomi dan Bisnis" value="FEB" />
        </Picker>
        <FlatList
          data={this.state.events}
          extraData={this.state}
          keyExtractor={(item, index) => item.nama}
          renderItem={({ item }) => (
            <Swipeout
              right={swipeBtns}
              autoClose={true}
              backgroundColor={"transparent"}
            >
              <CardView
                onPress={() =>
                  this.props.navigation.navigate('Details', {
                    nama: item.nama,
                    deskripsi: item.deskripsi,
                    img: item.img
                  })
                }
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: 'flex-start'
                  }}
                >
                  <CardTitle> {item.nama} </CardTitle>
                  <CardContent> {item.deskripsi} </CardContent>
                </View>
                <View
                  style={{
                    flex: 1,
                    alignItems: 'flex-end',
                    justifyContent: 'center'
                  }}
                >
                  <Image
                    style={{
                      width: 128,
                      height: 128
                    }}
                    source={item.img}
                  />
                </View>
              </CardView>
            </Swipeout>
          )}
        />
      </View>
    );
  }
}

export class DetailsScreen extends Component {
  static navigationOptions = {
    title: 'Details'
  };
  render() {
    const { params } = this.props.navigation.state;
    const namaEvent = params ? params.nama : null;
    const deskripsiEvent = params ? params.deskripsi : null;
    const imgEvent = params ? params.img : null;
    return (
      <View>
        <ScrollView>
          <View style={DetailStyles.view}>
            <Image style={DetailStyles.img} source={imgEvent} />
          </View>
          <View style={DetailStyles.view}>
            <Text style={[DetailStyles.text, DetailStyles.title]}>
              {namaEvent}
            </Text>
            <Text style={DetailStyles.text}> {deskripsiEvent} </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}
