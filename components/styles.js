import { StyleSheet } from 'react-native';

export const CardViewStyles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    fontSize: 25
  },
  text: {
    color: 'black'
  },
  card: {
    marginHorizontal: 5,
    borderBottomWidth: 1,
    borderColor: 'gray',
    padding: 18,
    backgroundColor: 'white',
    flexDirection: 'row',
    borderRadius: 5,
    elevation: 2
  }
});

export const DetailStyles = StyleSheet.create({
  view: {
    alignItems: 'center',
    padding: 10
  },
  img: {
    height: 300,
    width: 300
  },
  text: {
    color: 'black',
    fontSize: 16,
    textAlign: 'center'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 30
  }
});
